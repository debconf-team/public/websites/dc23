Dear {{ name }},

Our records indicate that you haven't settled your {{ WAFER_CONFERENCE_NAME }} invoice, for a
total of {{ total }} {{ currency }}.

If we don't receive payment by Friday September 1st, we will be treating your
attendance as unconfirmed. If you're unable to pay before then, or planning to
pay on-site, please reply to this email and let us know, ASAP.

https://{{ WAFER_CONFERENCE_DOMAIN }}/accounts/profile/

The invoice details are as follows:

Invoice number #{{ invoice_number }}

{{ invoice_details }}

Thanks for your support,

The {{ WAFER_CONFERENCE_NAME }} Registration Team
