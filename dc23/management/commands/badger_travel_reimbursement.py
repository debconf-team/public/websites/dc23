# -*- coding: utf-8 -*-

from csv import DictReader
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from django.template import Context, Template

from django.contrib.sites.models import Site

from bursary.models import Bursary

SUBJECT_TEMPLATE = Template(
    '[Action needed] Reimbursement process for {{ WAFER_CONFERENCE_NAME }} '
    'travel bursary recipients')

BODY_TEMPLATE = Template("""\
{% autoescape off %}
Dear {{ object.user.get_full_name }},

You have been granted a travel bursary for DebConf, and now is the time to
submit documentation for reimbursement. TL;DR: although this email is very
long, you will need all the information in it to get travel money back.

This year we are using a new reimbursement system, https://reimbursements.debian.net/
This is the first time the system is in use and we kindly ask you to look out
for problems and report any to bursaries@debconf.org.

Debian France will be processing reimbursements for DebConf-related expenses in
EUR (typically EU), and Software in the Public Interest (SPI) will handle
reimbursement requests in USD and other currencies. We have allocated your
request to {{ selected_to }}. If a different Trusted Organization would make
more sense for your request, please let us know by email.

The amount you requested is {{ object.travel_bursary }} {{ DEBCONF_BURSARY_CURRENCY }},
which is the maximum you'll be able to request for reimbursement.
We converted this to {{ converted_amount }} {{ currency }}, assuming you'd wish
to be reimbursed in {{ currency }}. If that's not correct, please let us know.

You can be reimbursed for:
1. The cheapest reasonable travel to and from the venue
2. Travel visa
3. Reasonable travel to obtain the visa

Receipts are mandatory for all of these. Paper receipts need to be scanned or
photographed clearly. If you photograph receipts, be sure they are well lit, in
focus, and cropped, so that they can be read easily. Multi-page receipts must
be combined into a single PDF.

The process is:
1. Your request already exists on the site. An account has been created with
   the email address {{ object.user.email }}; please set your password here:
   <http://reimbursements.debian.net/accounts/password/reset/>.
2. Log in and find your request here: <{{ request_url }}>.
3. For each receipt that you want to claim:
   1. Click "Add Receipt" to upload it.
   2. Attach the file. Each receipt must be a single file.
   3. Describe the receipt in a short sentence.
   4. Declare the receipt amount in a line at the bottom.
      If necessary, you can use multiple lines, but one line for the total
      should be sufficient in most cases.
4. Review your request, check that the receipt total is what you expect to be
   reimbursed, and is within the approved amount ({{ object.travel_bursary }} {{ DEBCONF_BURSARY_CURRENCY }}).
5. Check that the currency of the request is the one you want to be reimbursed in.
6. Click "Add Bank Details", and fill out your bank details.
7. Click "Submit for Reimbursement", and fill out the form.
8. Wait to get reimbursed, which can take a while as things frequently get
   processed in batches. If you don't see any progress after two weeks, please let
   us know by replying to this mail.

Special Cases:
* If you are making a partial claim, please note this in the description, and
  enter the amount you're claiming, instead of the invoiced total.
* If your receipt covers multiple attendees, please note this in the
  description, and if necessary, get in contact with us.

The deadline for filing your reimbursement is 10 December 2023.

If you have any questions, please be sure to contact us at bursaries@debconf.org
and we'll work your issues out.

Thanks for your cooperation!
--\u0020
The DebConf Bursaries team
{% endautoescape %}
""")


class Command(BaseCommand):
    help = 'Send an email to people with reimbursement instructions'

    def add_arguments(self, parser):
        parser.add_argument('requests.csv', type=open,
                            help="CSV of created reimbursement requests")
        parser.add_argument('--yes', action='store_true',
                            help='Actually send emails')

    def badger(self, row, bursary, conference_name, dry_run):
        context = {
            'object': bursary,
            'user': bursary.user.username,
            'to': '%s <%s>' % (bursary.user.get_full_name(),
                               bursary.user.email),
            'short_full_name': bursary.user.get_full_name().replace(' ', '')
                                                           .replace('-', '')
                                                           .replace("'", '')
                                                           .replace('.', ''),
            'DEBCONF_BURSARY_CURRENCY': settings.DEBCONF_BURSARY_CURRENCY,
            'WAFER_CONFERENCE_NAME': conference_name,
            'selected_to': row['selected_to'],
            'converted_amount': row['converted_amount'],
            'currency': row['currency'],
            'request_url': row['request_url'],
        }

        if dry_run:
            print('I would badger {to} (max = {object.travel_bursary})'
                  .format(**context))
            return

        from_email = 'bursaries@debconf.org'
        ctx = Context(context)
        subject = SUBJECT_TEMPLATE.render(ctx)
        body = BODY_TEMPLATE.render(ctx)

        msg = EmailMultiAlternatives(subject, body, to=[context['to']],
                                     from_email=from_email)

        msg.send()

    def handle(self, *args, **options):
        site = Site.objects.get()
        conference_name = site.name.replace(' ', '')
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        r = DictReader(options['requests.csv'])
        for row in r:
            try:
                bursary = Bursary.objects.get(
                    user__username=row['user.username'],
                    request_travel=True,
                    travel_status='accepted',
                    user__attendee__check_in__pk__isnull=False,
                    reimbursed_amount=0,
                )
            except Bursary.DoesNotExist:
                print(f"Could not locate bursary for {row['user.username']}")
                continue
            self.badger(row, bursary, conference_name, dry_run)
