# -*- coding: utf-8 -*-

from decimal import Decimal

from csv import DictReader
from django.core.management.base import BaseCommand

from bursary.models import Bursary


class Command(BaseCommand):
    help = 'Send an email to people with reimbursement instructions'

    def add_arguments(self, parser):
        parser.add_argument('reimbursements-paid.csv', type=open,
                            help="CSV of reimbursed requests")
        parser.add_argument('--yes', action='store_true',
                            help='Actually record updates')

    def record_reimbursement(self, bursary, amount, dry_run):
        if dry_run:
            print('I would record {to} received {amount}'
                  .format(
                    to=bursary.user.username,
                    amount=amount,
                  )
            )
            return
        bursary.reimbursed_amount = amount
        bursary.save()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        r = DictReader(options['reimbursements-paid.csv'])
        for row in r:
            try:
                bursary = Bursary.objects.get(
                    user__username=row['user.username'],
                    request_travel=True,
                    travel_status='accepted',
                    user__attendee__check_in__pk__isnull=False,
                    reimbursed_amount=0,
                )
            except Bursary.DoesNotExist:
                print(f"Could not locate bursary for {row['user.username']}")
                continue
            amount = int(Decimal(row['reimbursed']))
            if amount > 0:
                self.record_reimbursement(bursary, amount, dry_run)
