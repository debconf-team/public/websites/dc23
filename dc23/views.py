from datetime import timedelta
from django.http import JsonResponse
from django.utils import timezone
from wafer.schedule.models import ScheduleItem



def now_or_next(request, venue_id):
    talk_now = False
    talk_next = False
    reload_seconds = 60
    talk = None

    now = timezone.now()
    # now
    try:
        item = ScheduleItem.objects.filter(
            venue=venue_id,
            talk__isnull=False,
            slots__start_time__lte=now,
            slots__end_time__gte=now,
        ).order_by("slots__start_time")[0]
        duration = item.get_duration_minutes()
        end = item.get_start_datetime() + timedelta(minutes=duration)
        reload_seconds = (end - timezone.now()).total_seconds()

        talk_now = True
        talk = item.talk_id
    except IndexError:
        pass

    if not talk:
        # next
        try:
            item = ScheduleItem.objects.filter(
                venue=venue_id,
                talk__isnull=False,
                slots__start_time__gte=now
            ).order_by("slots__start_time")[0]
            reload_seconds = (item.get_start_datetime() - timezone.now()).total_seconds()
            talk_next = True
            talk = item.talk_id
        except IndexError:
            pass

    return JsonResponse({
        "now": talk_now,
        "next": talk_next,
        "reload_seconds": reload_seconds,
        "talk": talk,
    })
