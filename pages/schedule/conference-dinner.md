---
name: Conference Dinner
---

The conference dinner will be held at Bolgatty on September 14, Thursday.

Buses will collect us at the venue just after the photo session. Return buses
will also be provided.
